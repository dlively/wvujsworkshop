

var events = require('events');
var util = require("util");

function Cat(color) {
   this.color = color;
   events.EventEmitter.call(this);//think super() / this()

   this.sleep = function() {
       this.emit('sleep');
   }
}
//node.js helper
util.inherits(Cat, events.EventEmitter);
//same as Cat.prototype.__proto__ = events.EventEmitter.prototype

var felix = new Cat('black');
felix.on('sleep', function() {
   console.log('the cat is sleeping');
});

//....
setTimeout(function() {
   felix.sleep();
}, 2000);
