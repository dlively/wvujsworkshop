var http = require('http'),
    fs = require('fs');

var htmlFile = process.argv[2];

fs.readFile(htmlFile, function (err, html) {
    if (err) {
        throw err;
    }
    http.createServer(function(request, response) {
        response.writeHeader(200, {"Content-Type": "text/html"});
        response.write(html);
        response.end();
    }).listen(8000);
    console.log('server running http://localhost:8000');
});
