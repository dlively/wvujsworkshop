
function dosomething() {
    console.log('called do something');
}

//functions are first class objects!!!!

function doSomethingElse(cb) {
    console.log('doSomethingElse');
    cb();
}

doSomethingElse(dosomething);


doSomethingElse(function () {
    console.log('anonomous function called');
});


//arguments array/object?
function secretArray() {
    for (var i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
}

secretArray('a','b','c','d');
