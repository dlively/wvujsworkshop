/**
 Objects and basic data structures
 */

//create an array -- no need to define length
var data = ['a', 'b','c','d'];
console.log(data);
console.log(data[0]);
data.push('e');

for (var i = 0; i < data.length; i++) {
    console.log(data[i]);
}

console.log(data.join(','));

// object literals
var emptyObject = {};

var person = {
    name: 'Dan',
    location: 'Pittsburgh, PA',
    school: 'WVU',
    sayHello: function() {
        return 'Hello from ' + this.name;
    }
};

//dot notation ---
console.log(person.name + ' graduated from ' + person.school);

//subscript
console.log(person['location']);

console.log(person.sayHello());

//constructors

function Cat(name) {
    this.name = name;
}

//instance method
Cat.prototype.meow = function () {
    console.log(this.name + ' meow');
};

Cat.eat = function () {
    //static? methods
}

var myCat = new Cat('Felix');
myCat.meow();
Cat.eat();

//prototype === 'Awesome'
//add methods to objects at runtime

String.prototype.wvu = function () {
    return this + ' Mountaineers';
};

console.log("Lets Go".wvu());
