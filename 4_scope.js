
var a = 'hello';

function doSomething() {
    console.log(a);
}


doSomething();

function doSomethingElse() {
    var a = 'hi';
    console.log(a);
}

doSomethingElse();

console.log(a);


//closure
function someClosure() {
    var num = 0;
    return function() {
        console.log(num);
        num++;
    }
}

var closureFunction = someClosure();
closureFunction();
closureFunction();
closureFunction();
